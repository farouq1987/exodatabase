import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditeRoutingModule } from './edite-routing.module';
import { EditeComponent } from './edite.component';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [EditeComponent],
  imports: [
    CommonModule,
    EditeRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ]
})
export class EditeModule { }
