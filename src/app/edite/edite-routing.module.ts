import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditeComponent } from './edite.component';

const routes: Routes = [{ path: '', component: EditeComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditeRoutingModule { }
