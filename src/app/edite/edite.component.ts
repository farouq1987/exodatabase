import { Component, OnInit } from '@angular/core';
import {environment} from '../../environments/environment';
import {HotelModel} from '../models/hotel.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {HotelService} from '../services/hotel.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-edite',
  templateUrl: './edite.component.html',
  styleUrls: ['./edite.component.scss']
})
export class EditeComponent implements OnInit {
  uid: string;
  root: string = environment.api;
  hotel: HotelModel;
  myPut: FormGroup;
  constructor(
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private hotelService: HotelService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((param) => {
      this.myPut = this.fb.group({
        name: [null, [Validators.required]],
        roomNumbers: [null, [Validators.max(10)]],
        pool: [null, [Validators.requiredTrue]],
      });
    });
  }


  apdateHotel(): void {
    this.activatedRoute.paramMap.subscribe((params) => {
      this.uid = params.get('id');
    });
    this.hotel = this.myPut.value;
    this.hotelService.apdateHotel$(this.hotel, this.uid)
      .subscribe();
  }

  sendObject() {
    if (this.myPut.valid)
    {
      console.log(this.myPut.value);
    }
    else {
      alert('le formemaire est invalid!!!');
    }
  }
}
