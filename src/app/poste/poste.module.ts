import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PosteRoutingModule } from './poste-routing.module';
import { PosteComponent } from './poste.component';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [PosteComponent],
    imports: [
        CommonModule,
        PosteRoutingModule,
        ReactiveFormsModule
    ]
})
export class PosteModule { }
