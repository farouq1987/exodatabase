import { Component, OnInit } from '@angular/core';
import {HotelModel} from '../models/hotel.model';
import {environment} from '../../environments/environment';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {HotelService} from '../services/hotel.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-poste',
  templateUrl: './poste.component.html',
  styleUrls: ['./poste.component.scss']
})
export class PosteComponent implements OnInit {

  root: string = environment.api;
  hotel: HotelModel;
  myPoste: FormGroup;
  constructor(
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private hotelService: HotelService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((param) => {
    this.myPoste = this.fb.group({
      name: [null, [Validators.required]],
      roomNumbers: [null, [Validators.max(10)]],
      pool: [null, [Validators.requiredTrue]],
    });
    });
  }


  creatHotel(): void {
        this.hotel = this.myPoste.value;
        this.hotelService.creatNewHotel$(this.hotel)
          .subscribe();
  }

  sendObject() {
    if (this.myPoste.valid)
    {
      console.log(this.myPoste.value);
    }
    else {
      alert('le formemaire est invalid!!!');
    }
  }
}
