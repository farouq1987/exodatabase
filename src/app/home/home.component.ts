import {Component, OnDestroy, OnInit} from '@angular/core';
import {HotelService} from '../services/hotel.service';
import {HotelModel} from '../models/hotel.model';
import {combineLatest, Subject} from 'rxjs';
import {takeUntil, tap} from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  hotels: HotelModel[];
  destroy$: Subject<boolean> = new Subject();

  constructor(
    private hotelService: HotelService,
  ) { }

  ngOnInit(): void {
    combineLatest([
      this.hotelService.getAllHostels$()
        .pipe(
          tap((liste) => this.hotels = liste ),
        ),
    ])
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe();

  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
