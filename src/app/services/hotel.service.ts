import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HotelModel} from '../models/hotel.model';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HotelService {
 root: string = environment.api;
  constructor(
    private httpClient: HttpClient,
  ) { }

  getAllHostels$(): Observable<HotelModel[]> {
    return (this.httpClient
      .get(this.root + 'hostels') as Observable<HotelModel[]>);
  }

  getHotelById$(hotelId: string): Observable<HotelModel> {
    return this.httpClient
      .get(this.root + 'hostels/' + hotelId) as Observable<HotelModel>;
  }

  creatNewHotel$(hotel: HotelModel): Observable<HotelModel> {
    return (this.httpClient
      .post(this.root + 'hostels' , hotel) as Observable<HotelModel>)
      .pipe(
        tap(x => console.log(x))
      );
  }

  apdateHotel$(hotel: HotelModel, hotelId: string): Observable<HotelModel> {
    return (this.httpClient
      .patch(this.root + 'hostels/' + hotelId , hotel) as Observable<HotelModel>)
      .pipe(
        tap(x => console.log(x))
      );
  }


  deletHotel$(hotelId: string): Observable<string> {
  return (this.httpClient
    .delete(this.root + 'hostels/' + hotelId) as Observable<string>)
    .pipe(
      tap(x => console.log(x))
    );
  }
}
