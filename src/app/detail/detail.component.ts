import {Component, OnDestroy, OnInit} from '@angular/core';
import {HotelModel} from '../models/hotel.model';
import {Subject} from 'rxjs';
import {HotelService} from '../services/hotel.service';
import {takeUntil, tap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy{
  hotels: HotelModel;
  destroy$: Subject<boolean> = new Subject();
  uid: string;
  constructor(
    private hotelService: HotelService,
    private activatedRoute: ActivatedRoute,
  ) { }


  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params) => {
      this.uid = params.get('id');
    });
    this.hotelService.getHotelById$(this.uid)
      .pipe(
        tap((hotelById) => this.hotels = hotelById )
      )
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe();

  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
