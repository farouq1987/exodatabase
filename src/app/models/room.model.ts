export interface RoomModel {
    id: number;
    size: number;
    roomName: string;
}
