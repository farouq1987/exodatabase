export interface MenuModel {
  url: string,
  name: string,
  minimalAge: number,
}

