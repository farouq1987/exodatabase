import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeletComponent } from './delet.component';

const routes: Routes = [{ path: '', component: DeletComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeletRoutingModule { }
