import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeletRoutingModule } from './delet-routing.module';
import { DeletComponent } from './delet.component';


@NgModule({
  declarations: [DeletComponent],
  imports: [
    CommonModule,
    DeletRoutingModule
  ]
})
export class DeletModule { }
