import {Component, OnDestroy, OnInit} from '@angular/core';
import {HotelService} from '../services/hotel.service';
import {ActivatedRoute} from '@angular/router';
import {HotelModel} from '../models/hotel.model';
import {Subject} from 'rxjs';
import {takeUntil, tap} from 'rxjs/operators';

@Component({
  selector: 'app-delet',
  templateUrl: './delet.component.html',
  styleUrls: ['./delet.component.scss']
})
export class DeletComponent implements OnInit, OnDestroy {
  hotels: HotelModel;
  destroy$: Subject<boolean> = new Subject();
  uid: string;
  constructor(
    private hotelService: HotelService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params) => {
      this.uid = params.get('id');
    });
    this.hotelService.getHotelById$(this.uid)
      .pipe(
        tap((hotelById) => this.hotels = hotelById )
      )
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe();

  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }


  deletHotel(): void {
    this.hotelService.deletHotel$(this.uid)
      .subscribe();
  }
}




