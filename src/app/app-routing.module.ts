import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'hostels', loadChildren: () => import('./home/home.module').then(m => m.HomeModule) },
  { path: 'hostels/:id/detail', loadChildren: () => import('./detail/detail.module').then(m => m.DetailModule) },
  { path: 'hostels/:id/edite', loadChildren: () => import('./edite/edite.module').then(m => m.EditeModule) },
  { path: '', redirectTo: '/hostels', pathMatch: 'full'  },
  { path: 'poste', loadChildren: () => import('./poste/poste.module').then(m => m.PosteModule) },
  { path: 'hostels/:id/delet', loadChildren: () => import('./delet/delet.module').then(m => m.DeletModule) },
  ];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
